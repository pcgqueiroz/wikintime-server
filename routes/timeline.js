const express = require('express');
const cors = require('cors');
const TimelineEvents = require('../models/timelineEvent.model');

const timelineRouter = express.Router();

timelineRouter.use(express.json());

timelineRouter.route('/')
    .get(
        cors(),
        (req, res, next) => {
            TimelineEvents.find({})
                .then((timelineEvents) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(timelineEvents);
                })
                .catch((err) => next(err));
        },
    )
    .post(
        cors(),
        (req, res, next) => {
            TimelineEvents.create(req.body)
                .then((timelineEvent) => {
                    console.log('Timeline Event Created:\n', timelineEvent);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(timelineEvent);
                })
                .catch((err) => next(err));
        },
    )
    .put(
        (req, res, next) => {
            res.statusCode = 403;
            res.end('PUT operation not supported on /timeline');
        },
    )
    .delete(
        (req, res, next) => {
            res.statusCode = 403;
            res.end('DELETE operation not supported on /timeline');
        },
    );

    module.exports = timelineRouter;