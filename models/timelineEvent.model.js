const mongoose = require('mongoose');

const timelineEventSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
        },
        introduction: {
            type: String,
            required: true,
        },
        yearStart: {
            type: String,
            required: true,
        },
        yearEnd: {
            type: String,
            default: '',
        },
        imageUrl: {
            type: String,
            default: '',
        },
        imageCaption: {
            type: String,
            default: '',
        },
        url: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true,
    },
);

module.exports = mongoose.model('TimelineEvent', timelineEventSchema);