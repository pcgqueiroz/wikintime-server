const Schema = require('mongoose').Schema;

const dateTimeSchema = {
    year: {
        type: Number,
        required: true,
    },
    month: {
        type: Number,
        min: 1,
        max: 12,
    },
    day: {
        type: Number,
        min: 1,
        max: 31,
    },
    hour: {
        type: Number,
        min: 0,
        max: 59,
    },
    minute: {
        type: Number,
        min: 0,
        max: 59,
    },
    seconds: {
        type: Number,
        min: 0,
        max: 59,
    },
};

const timelineEventSchema = new Schema(
    {
        source: {
            type: String,
            required: true,
        },
        pageTitle: {
            type: String,
            required: true,
        },
        date: {
            start: dateTimeSchema,
            end: dateTimeSchema,
        },
    },
    {
        timestamps: true,
    }
);

const timelineDataSchema = new Schema(
    {
        timelineId: {
            type: ObjectId,
        },
        data: [timelineEventSchema],
    }
);

module.exports = mongoose.model('TimelineData', timelineDataSchema);