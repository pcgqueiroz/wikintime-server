const Schema = require('mongoose').Schema;

const timelineInfoSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
        },
        lang: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model('TimelineInfo', timelineInfoSchema);